class Coche:
    def __init__(self, color, marca, modelo, matricula, vel):
        self.__color= color
        self.__marca = marca
        self.__modelo = modelo
        self.__matricula = matricula
        self.__vel = vel
    
    def Imprimircaracteristicas(self):
        print ("El coche es de color {color} de marca {marca} en el modelo {modelo} con la matricula {matricula}".format(color=self.__color, marca=self.__marca, modelo=self.__modelo, matricula=self.__matricula))
    
    def Acelerar(self):
        return self.__vel + 10
    
    def Frenar(self):
        return self.__vel -10
    

#Programa
    
coche = Coche('rojo','BMW', 'X5', '1970MHV', 20 )
coche.Imprimircaracteristicas()
print('El coche acelera y queda a una velocidad de:' +str(coche.Acelerar()) + 'm/s')
print('El coche frena y queda a una velocidad de:' +str(coche.Frenar()) + 'm/s')