from coche import Coche
import unittest

class TestCoche(unittest.TestCase):

    def testcaracteristicas(self):
        coche = Coche('rojo','BMW', 'X5', '1970MHV', 20 )
        caracteristicas = coche.Imprimircaracteristicas()
        self.assertEqual(caracteristicas, None)

    def testacelerar(self):
        coche = Coche('rojo','BMW', 'X5', '1970MHV', 20 )
        acelerar = coche.Acelerar()
        self.assertEqual(acelerar, 30)

    def testfrenar(self):
        coche = Coche('rojo','BMW', 'X5', '1970MHV', 20 )
        frenar = coche.Frenar()
        self.assertEqual(frenar, 10)
        

if __name__== "__main__":
    unittest.main()
        